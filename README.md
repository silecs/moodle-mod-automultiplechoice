Auto Multiple Choice (AMC) for Moodle
=====================================

This is an interface to use [AMC](https://www.auto-multiple-choice.net) within Moodle.

This Moodle module is intended for use with the moodle-local-questionssimplified module.
The former handles the paper quizzes built from Moodle multiple-choice questions,
while the laer provides a simple interface to type these questions.

It is also recommended to install the filter module moodle-filter-amc.

This module was created for the *service TICE de l'Université Pierre-Mendès-France - Grenoble 2, Sciences sociales et humaines*.
It was completed for the *Université de Nouvelle-Calédonie* and tested with Moodle 3.7.

Features
--------

An admin can:

* Define complex scoring rules.
* Define how to match a student number from the paper test to a student number in Moodle.
* Define various default text for paper tests.

A teacher can:

* Configure various [AMC](http://home.gna.org/auto-qcm/) parameters (randomization, etc).
* Select questions and assign a number of points to each one.
* Select a scoring rule.
* Configure various texts that appear on print sheets.
* Prepare PDF question sheets and answer sheets.
* Upload the scanned answer shhets and automatically grade them.
* See the grades in Moodle or download them.
* Download graded copies with PDF annotations.

### Warning

The interface is not fully localized.
There will be French texts here and there.


## Install

See <documentation/install.md> for instructions.

