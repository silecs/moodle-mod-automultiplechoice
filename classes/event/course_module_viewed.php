<?php
/**
 * @package    mod_automultiplechoice
 * @copyright  2018 Silecs
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_automultiplechoice\event;

defined('MOODLE_INTERNAL') || die();

class course_module_viewed extends \core\event\course_module_viewed {
    protected function init() {
        $this->data['objecttable'] = 'automultiplechoice';
        parent::init();
    }

    public static function triggerEvent($cm, $context, $course, $activityrecord = null) {
        $event = self::create([
            'objectid' => $cm->instance,
            'context' => $context,
        ]);
        $event->add_record_snapshot('course', $course);
        if ($activityrecord) {
            if ($activityrecord instanceof stdClass) {
                $event->add_record_snapshot($cm->modname, $activityrecord);
            } else {
                $event->add_record_snapshot($cm->modname, (object) get_object_vars($activityrecord));
            }
        }
        $event->trigger();
    }
}
