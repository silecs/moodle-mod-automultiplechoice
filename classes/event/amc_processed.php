<?php
/**
 * @package    mod_automultiplechoice
 * @copyright  2018 Silecs
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_automultiplechoice\event;

defined('MOODLE_INTERNAL') || die();

class amc_processed extends \core\event\base {
    protected function init() {
        $this->data['crud'] = 'u';
        $this->data['edulevel'] = self::LEVEL_TEACHING;
        $this->data['objecttable'] = 'automultiplechoice';
    }

    public static function get_name() {
        return "process auto-multiple-choice";
    }

    public function get_description() {
        return "The user with id {$this->userid} ran an AMC process on the quizz with id {$this->other[0]}.";
    }

    public static function triggerEvent($quizz, $action, $msg) {
        $cm = \get_coursemodule_from_instance('automultiplechoice', $quizz->id);
        $event = self::create([
            'objectid' => $cm->instance,
            'context' => \context_module::instance($cm->instance),
            'other' => [$quizz->id, $action, $msg],
        ]);
        $event->trigger();
    }
}
