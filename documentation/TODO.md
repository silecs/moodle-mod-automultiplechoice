# TODO


- [x] rédiger une doc d'installation
- [ ] rédiger une doc d'utilisation


## Page "Settings"

- [ ] vérifier que le programme auto-multiple-choice est présent `/usr/lib/AMC/perl/AMC-prepare.pl`
- [ ] traduction FR pour 'instructions' 'scoringrules'

## Page de création d'activité

- [x] La consigne prédéfinie n'est pas insérée. `tinyMCE is not defined` dans `mainform.js:23`
- [x] Corriger la traduction FR incohérente "au début de la consigne, et la consigne "
- [ ] Documenter "Copies anonymes" "Zone d'identification" "Montrer les points" "Marque pour réponses multiples"
- [ ] Rendre compréhensible la doc de "Personnalisation de la mise en pages"

## questions.php

- [x] localisation de DataTables
- [ ] DataTables : passer à un chargement par AJAX
- [ ] Ajouter un bouton "Refresh"
- [x] Les questions héritées des niveaux supérieurs sont absentes

## questionssimplified/`edit_wysiwyg.php`

- [ ] Ne pas introduire l'aide par le mot "Consigne"
- [ ] Dans l'aide, donner un exemple
- [ ] Retirer "Le titre" "La doc" des traductions

## question/edit.php

- [ ] Par défaut, ne pas montrer le texte de la question
- [ ] Traduire "The default category for questions shared in context 'System'."
- [ ] La récursivité ne fonctionne par pour le niveau "Système" ?

## scoringsystem.php

- [ ] Documenter chaque champ et le bouton "Répartir..."
- [x] "8/8" reste en fond rouge
- [x] `add_to_log()` est obsolète

## automultiplechoice/documents.php

- [ ] documenter les boutons
