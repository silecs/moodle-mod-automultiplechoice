Débogage du module automultiplechoice
=====================================

## Vérifier que les programmes sont correctement installés

Lancer manuellement `auto-multiple-choice` en mode graphique,
ou certains composants en mode CLI.
Par exemple, sous Debian `/usr/lib/AMC/perl/AMC-annotate.pl`.

## Répertoire du QCM

Chaque QCM a son répertoire dans moodledata, placé dans
`local/automultiplechoice/automultiplechoice_ID`.

Tous les documents (scans, corrections, CSV, etc) sont là.

## Logs

Les logs stockés dans Moodle sont minimaux.

Dans le répertoire du QCM, `AMC_commands.log`
contient toutes les commandes AMC exécutés, avec horodatage et affichage du résultat.

Si le mode développeur de Moodle est actif (`$CFG->debug` est non vide),
alors le répertoire contriendra des logs détaillées de certaines opérations d'AMC.

