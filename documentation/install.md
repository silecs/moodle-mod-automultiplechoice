Installing `mod_automultiplechoice`
===================================

On a Debian 10 server with a Moodle 3.X instance (tested up to 3.7).

1. `apt install automultiplechoice texlive-xetex poppler-utils`

2. Install the plugin `mod_automultiplechoice`:
   `git clone https://gitlab.com/silecs/moodle-mod-automultiplechoice.git mod/mod_automultiplechoice)`

3. Log into Moodle as an admin and configure the plugin.
