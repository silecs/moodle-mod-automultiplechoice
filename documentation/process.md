Description du processus d'autpmultiplechoice (AMC)
===================================================

Répertoires
-----------

tous les chemins sont relatifs au répertoire des données de l'instance de l'activité :
`moodledata/local/automultiplechoice/automultiplechoice_000ID`.

Par exemple, `moodledata-qcm/local/automultiplechoice/automultiplechoice_00013`.


Sujets (PDF)
------------

* Préparation des sujets : amcCreatePdf + amcMeptex
* Verrouillage : amcImprime


amcCreatePdf
:	Prépare les sujets temporaires.

	Entrée
	:	* fichier `prepare-source.tex`
		* paramètre variable `n-copies`
	Sortie
	:	* crée `prepare-calage.xy`
		* crée `sujet-NORM.pdf`
		* crée `catalog-NORM.pdf`
		* crée `corrige-NORM.pdf`
		* crée `amc-compiled.*`

amcMeptex
:	[capture, layout] créés

	Entrée
	:	* fichier `prepare-calage.xy`
	Sortie
	:	* modifie `data/capture.sqlite`
		* modifie `data/layout.sqlite`

amcImprime
:	Prépare l'archive contenant les sujets à imprimer.
	Complément optionnel aux deux précédents appel, lancé en cas de verrouillage.

	Entrée
	:	* `sujet-NORM.pdf`
	Sortie
	:	* crée `imprime/sujet-%e.pdf`
		* crée `sujets-NORM.zip`



Dépôt de copies
---------------

* Dépôt : enregistrement temporaire dans `/tmp/FILENAME.pdf`
* Traitement : deleteGrades() + amcMeptex + amcGetimages + amcAnalyse


amcMeptex
:	Cf Sujets.

amcGetimages
:	Convertit les images vers le format ppm.

	Entrée
	:	* fichier déposé `/tmp/FILENAME.pdf`
	Sortie
	:	* crée `scanlist`
        * crée `scans/SCANNAME-page-*.ppm` (pages brutes)

amcAnalyse
:	Extrait des images les parties qui contiennent des données.

	Entrée
	:	* fichier `scanlist` produit par amcGetimages
		* paramètre variable `bw-threshold`
	Sortie
	:	* crée `cr/page-*.jpg`
		* crée `cr/name-*.jpg`
		* modifie `data/capture.sqlite` (blobs images)


Notation
--------

amcPrepareBareme -> amc-compiled.* : pdf,xy créés ; .amc,.log mis à jour
    V               [capture] modifié ; [scoring] créé
    V

	Entrée
	:	* 
	Sortie
	:	* 
amcNote          -> [capture, scoring] modifiés

	Entrée
	:	* 
	Sortie
	:	* 
    V
    V
amcExport         -> @/exports/scores.csv  ;  amc-compiled.log,.pdf modifiés
    V                [association] créé ; [capture, scoring] modifiés

	Entrée
	:	* 
	Sortie
	:	* 
    V
writeFileWithIdentifiedStudents  -> @exports/ : score_names.csv, student_list.csv
    V

	Entrée
	:	* 
	Sortie
	:	* 
    V
amcAssociation  ???

	Entrée
	:	* 
	Sortie
	:	* 

    V
    V
amcAnnotePdf
[ amcAnnote   ]   -> @/cr/corrections/jpg/page-*.jpg

	Entrée
	:	* 
	Sortie
	:	* 
    V
    V
amcAssociation  ???


note : l'appel à amcAssociation reste à préciser
